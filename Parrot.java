public class Parrot {
    private String name;
    private String color;
    private String species;

    public Parrot(String name, String color, String species) {
        this.name = name;
        this.color = color;
        this.species = species;
    }
    public void speak() {
        System.out.println("*squawk* My name is " + name + ".");
    }
    public void molt() {
        System.out.println("*squawk* There are " + color.toLowerCase() + " feathers all over the place! *squawk*");
    }

    public String getName() {
        return this.name;
    }

    public String getColor() {
        return this.color;
    }

    public String getSpecies() {
        return this.species;
    }

    public void setName(String name) {
        this.name = name;
    }
}