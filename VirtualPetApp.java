import java.util.Scanner;

public class VirtualPetApp {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Parrot[] parrots = new Parrot[1];

        for(int i = 0; i < parrots.length; i++) {
            System.out.println("Enter name:");
            String name = reader.nextLine();

            System.out.println("Enter color:");
            String color = reader.nextLine();

            System.out.println("Enter species:");
            String species = reader.nextLine();

            parrots[i] = new Parrot(name, color, species);
        }
        System.out.println();
        System.out.println("name: " + parrots[0].getName());
        System.out.println("color: " + parrots[0].getColor());
        System.out.println("species: " + parrots[0].getSpecies());
        System.out.println();

        System.out.println("Enter new name: ");
        String newName = reader.nextLine();
        parrots[0].setName(newName);
        
        System.out.println();
        System.out.println("name: " + parrots[0].getName());
        System.out.println("color: " + parrots[0].getColor());
        System.out.println("species: " + parrots[0].getSpecies());
        
        reader.close();
    }
}